<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class GoiMiddlewareTest extends TestCase
{
	/**
     * A basic test example.
     *
     * @return void
     */
    public function testUnauthorized()
    {
	    $mocks = new Mocks();
	    $middleware = new \App\Http\Middleware\GoiMiddleware();
	    $response = $middleware->handle($mocks->mockApiRequest(true), function () {});
	    $this->assertNull($response);
    }

    public function testAuthorized()
    {
	    $mocks = new Mocks();
	    $middleware = new \App\Http\Middleware\GoiMiddleware();
	    $response = $middleware->handle($mocks->mockApiRequest(false), function () {});
		$this->assertEquals($response->getContent(), 'Unauthorized.');
    }
}
