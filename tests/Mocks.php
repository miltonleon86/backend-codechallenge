<?php

use Illuminate\Http\Request;

/**
 * Class Mocks
 */
class Mocks extends TestCase
{

	/**
	 * @param $middlewareResponse
	 *
	 * @return \Illuminate\Http\Request
	 */
	public function mockApiRequest($middlewareResponse): Request
	{
		$request = $this->createMock(Request::class);
		$request->expects($this->at(0))
			->method('hasHeader')
			->with('Authorization')
			->willReturn($middlewareResponse);

		return $request;
	}
}