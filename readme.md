# GOI Test

## Antes de comenzar.

- Gracias por la oportunidad de desarrollar este Test, pues no tengo 
ninguna experiencia en Laravel o Lumen que es lo micro framework que 
decidi usar ya que es basado en Laravel y usa el mismo ORM (Eloquent).

- Hasta ahora me llevo muy buenas impresiones, comparado con otros 
micro frameworks como Silex o Phalcon.

- Utilice la oportunidad no solo para desarrollar el challenge sino 
tambien para desarrollar mi propio Lumen Template.

- Gracias.

- TODO
```$bash
Improve Test and coverage
```

- Done 
```$bash
Routers
Controller
Goi Service Provider
Goi Service
Repositories
Models
Migrations
Seeds
Tests (Only the middleware)
Arquitecture

getOrders Request.
getOrders{driverId} Request.
insertOrders.
AssignOrders.

Domains (For domain driven design -> check how can I apply it)
```
   

## Goi Test Installations Steps
* Make sure you're not using port 80 or 3306 in other apps. 

* Add Goi to you local hosts
```$bash
In 
/etc/hosts
Add 
127.0.0.1       test.goi.develop
```

* Build Images.
```$bash
docker-compose build --no-cache
```

* Start All Containers.
```$bash
docker-compose up -d
```

* Run Composer.
```$bash
docker exec backend-codechallenge_php_1 composer install
```
* Run Default Migrations to set some drivers, Seeds are not required.
```$bash
docker exec backend-codechallenge_php_1 php artisan migrate
docker exec backend-codechallenge_php_1 php artisan db:seed
```

* Unit Tests.
```$bash
docker exec backend-codechallenge_php_1 php ./vendor/bin/phpunit tests
```

* Ready to perform our first request.
```$bash
http://test.goi.develop/goi/get-orders

Response:
Unauthorized.
```

* We need to add an Authorization Header to each request since 
all request are behind the Goi Middleware Group, no free entrance :).

* Ready Requests:
```$bash
Get All Active Orders (We could think about setting a timeframe).

curl -X GET \
  http://test.goi.develop/goi/get-orders \
  -H 'authorization: 123' \
  -H 'cache-control: no-cache' \
  -H 'postman-token: 502373ff-b55b-ff10-0df5-a56494eddd4e'

OR

Use Postman :)  

Response:
[
    {
        "orderId": 456,
        "driverId": 123,
        "status": 0,
        "clientName": "Melanie",
        "clientSurname": "Trump",
        "email": "thetrumps@whitehause.com",
        "phone": "+492323232",
        "shippingAddress": "1600 Pennsylvania Ave NW, Washington, DC 20500, EE. UU",
        "deliveryDate": "2018-12-18 20:41:22",
        "hourFrom": 12,
        "hourTo": 20,
        "createdAt": "2018-12-18 20:41:22",
        "updatedAt": "2018-12-18 20:41:22"
    }
]
```

```$bash
Get All Active Orders Related to a Driver.

curl -X GET \
  http://test.goi.develop/goi/get-orders/123 \
  -H 'authorization: 123' \
  -H 'cache-control: no-cache' \
  -H 'postman-token: 502373ff-b55b-ff10-0df5-a56494eddd4e'

OR

Use Postman :)  

Response:
[
    {
        "driverId": 123,
        "driverName": "testDriver",
        "createdAt": "2018-12-18 21:10:25",
        "updatedAt": "2018-12-18 21:10:25",
        "orders": [
            {
                "orderId": 456,
                "driverId": 123,
                "status": 0,
                "clientName": "Melanie",
                "clientSurname": "Trump",
                "email": "thetrumps@whitehause.com",
                "phone": "+492323232",
                "shippingAddress": "1600 Pennsylvania Ave NW, Washington, DC 20500, EE. UU",
                "deliveryDate": "2018-12-18 21:10:25",
                "hourFrom": 12,
                "hourTo": 20,
                "createdAt": "2018-12-18 21:10:25",
                "updatedAt": "2018-12-18 21:10:25"
            }
        ]
    }
]
```

* Set Order

```$bash
curl -X POST \
  http://test.goi.develop/goi/set-order/ \
  -H 'authorization: 123' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: ccfd9dc2-8f3c-05ad-0055-bb4dbcf0f639' \
  -d '{"clientName":"Melanie","clientSurname":"Trump","email":"thetrumps@whitehause.com","phone":"+492323232","shippingAddress":"1600 Pennsylvania Ave NW, Washington, DC 20500, EE. UU","deliveryDate":"2018-12-19","hourFrom":12,"hourTo":20}'

The response should be the order.
```

* Assign Orders (This will assign unassigned orders to the best 
driver. Best driver is there who does not have orders
 or the ohas less orders for order deliveryDate)
```$bash
curl -X PUT \
  http://test.goi.develop/goi/assign-orders/ \
  -H 'authorization: 123' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 967d55a8-c414-7472-37e8-ba17d6f70db7'
```









