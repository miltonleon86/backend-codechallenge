<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 12/18/18
 * Time: 9:06 PM
 */

namespace App\Services;

use App\DTO\Order;
use App\Repositories\DriverRepository;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

/**
 * Class GoiService
 *
 * @package App\Services
 */
class GoiService
{
	/** @var OrderRepository $orderRepository */
	private $orderRepository;

	/** @var DriverRepository $driverRepository */
	private $driverRepository;

	public function __construct(OrderRepository $orderRepository, DriverRepository $driverRepository)
	{
		$this->orderRepository  = $orderRepository;
		$this->driverRepository = $driverRepository;
	}

	/**
	 *
	 * @return string
	 */
	public function getActiveOrders(): string
	{
		return $this->orderRepository->getActiveOrders();
	}

	public function getDriverActiveOrders($driverId): string
	{
		return $this->driverRepository->getDriverActiveOrders($driverId);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return string
	 */
	public function setOrder(Request $request): string
	{
		$orderDto = new Order($request);
		$this->orderRepository->setOrder($orderDto);

		return $request;
	}

	/**
	 * @return string
	 */
	public function assignOrders(): string
	{
		/** AllActive Orders */
		$activeOrders = json_decode($this->getActiveOrders(), true);

		/** We need to iterate in order to get the correct offer date */
		foreach ($activeOrders as $activeOrder) {
			$driversWithNoOrders = $this->driverRepository->getDriversWithNoOrders($activeOrder['deliveryDate']);

			/** First assign order to first driver with no orders */
			if (count($driversWithNoOrders) > 0) {
				$this->orderRepository->assignOrderToDriver($activeOrder, $driversWithNoOrders[0]);
				continue;
			}

			/** If all drivers have orders assign it to the driver with less orders */
			$driversWithLessOrders = $this->driverRepository->getDriversWithLessOrders($activeOrder['deliveryDate']);
			$this->orderRepository->assignOrderToDriver($activeOrder, (array)$driversWithLessOrders[0]);
		}

		return json_encode(['Assigned' => 'OK']);
	}
}