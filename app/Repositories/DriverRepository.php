<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 12/18/18
 * Time: 9:14 PM
 */

namespace App\Repositories;


use App\Driver;
use Illuminate\Support\Facades\DB;

/**
 * Class DriverRepository
 *
 * @package App\Repositories
 */
class DriverRepository
{

	private $startDate;

	private $endDate;

	public function getDriverActiveOrders($driverId)
	{
		$driver = Driver::where('driverId', '=', (int)$driverId)->with([
			'orders' => function ($query) {
				$query->where('status', '=', false);

			},
		])->get();

		return $driver;
	}

	/**
	 * We get all drivers with no orders for the day
	 *
	 * @param $initDate
	 *
	 * @return
	 */
	public function getDriversWithNoOrders($initDate): array
	{
		$this->startDate = $initDate;
		$this->endDate   = date('Y-m-d', strtotime($initDate . ' +1 day'));

		$drivers = Driver::whereNotIn('driverId', function ($query) {
			$query->select('driverId')
				->distinct()
				->from('orders')
				->where('driverId', '!=', null)// Be sure you only analyze available orders.
				->whereBetween('deliveryDate', [
					$this->startDate, $this->endDate,
				]); // Analyze only for the current Order date.
		})->get();

		return $drivers->toArray();
	}

	/**
	 * We get all drivers less orders for the day.
	 *
	 * @param $initDate
	 *
	 * @return array
	 */
	public function getDriversWithLessOrders($initDate): array
	{
		$this->startDate = $initDate;
		$this->endDate   = date('Y-m-d', strtotime($initDate . ' +1 day'));

		return  DB::select("
			SELECT 
			    driverId
			FROM
			    orders
			WHERE
			    driverId != '' 
			AND deliveryDate BETWEEN '$this->startDate' AND '$this->endDate'
			GROUP BY driverId
			ORDER BY COUNT(driverId) ASC;
		");
	}
}