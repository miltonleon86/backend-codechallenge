<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 12/18/18
 * Time: 8:50 PM
 */

namespace App\Repositories;


use App\DTO\Order as OrderDTO;
use App\Order;

/**
 * Class OrderRepository
 *
 * @package App\Repositories
 */
class OrderRepository
{
	/**
	 * @return string
	 */
	public function getActiveOrders(): string
	{
		return Order::all()
			->where('status', '=', false)
			->where('driverId', '=', null);
	}

	/**
	 * @param \App\DTO\Order $orderDTO
	 *
	 * @return void
	 */
	public function setOrder(OrderDTO $orderDTO): void
	{
		$order = new Order();
		$order->setRawAttributes($orderDTO->toArray());
		$order->save();
	}

	/**
	 * @param \App\Order $order
	 * @param \App\Driver $driver
	 */
	public function assignOrderToDriver($order, $driver)
	{
		$orderToUpdate = Order::find($order['id']);
		$orderToUpdate->driverId = $driver['driverId'];
		$orderToUpdate->save();
	}
}