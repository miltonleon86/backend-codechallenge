<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 12/18/18
 * Time: 3:33 PM
 */

namespace App\DTO;


use Illuminate\Http\Request;

class Order
{
	/** @var string $orderId */
	private $orderId;

	/** @var string $driverId */
	private $driverId;

	/** @var int $status */
	private $status;

	/** @var string $clientName */
	private $clientName;

	/** @var string $clientSurname */
	private $clientSurname;

	/** @var string $email */
	private $email;

	/** @var string $phone */
	private $phone;

	/** @var string $shippingAddress */
	private $shippingAddress;

	/** @var string $deliveryDate */
	private $deliveryDate;

	/** @var int $hourFrom */
	private $hourFrom;

	/** @var int $hourTo */
	private $hourTo;

	/**
	 * Order constructor.
	 *
	 * @param \Illuminate\Http\Request|null $request
	 */
	public function __construct(Request $request = null)
	{
		if ($request instanceof Request) {
			$this->orderId = uniqid();
			$this->status = false;
			$this->clientName = $request->post('clientName');
			$this->clientSurname = $request->post('clientSurname');
			$this->email = $request->post('email');
			$this->phone = $request->post('phone');
			$this->shippingAddress = $request->post('shippingAddress');
			$this->deliveryDate = $request->post('deliveryDate');
			$this->hourFrom = $request->post('hourFrom');
			$this->hourTo = $request->post('hourTo');
		}
	}

	/**
	 * @return string
	 */
	public function getOrderId(): string
	{
		return $this->orderId;
	}

	/**
	 * @param string $orderId
	 */
	public function setOrderId(string $orderId): void
	{
		$this->orderId = $orderId;
	}

	/**
	 * @return string
	 */
	public function getDriverId(): string
	{
		return $this->driverId;
	}

	/**
	 * @param string $driverId
	 */
	public function setDriverId(string $driverId): void
	{
		$this->driverId = $driverId;
	}

	/**
	 * @return int
	 */
	public function getStatus(): int
	{
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus(int $status): void
	{
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getClientName(): string
	{
		return $this->clientName;
	}

	/**
	 * @param string $clientName
	 */
	public function setClientName(string $clientName): void
	{
		$this->clientName = $clientName;
	}

	/**
	 * @return string
	 */
	public function getClientSurname(): string
	{
		return $this->clientSurname;
	}

	/**
	 * @param string $clientSurname
	 */
	public function setClientSurname(string $clientSurname): void
	{
		$this->clientSurname = $clientSurname;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail(string $email): void
	{
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getPhone(): string
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone(string $phone): void
	{
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getShippingAddress(): string
	{
		return $this->shippingAddress;
	}

	/**
	 * @param string $shippingAddress
	 */
	public function setShippingAdress(string $shippingAddress): void
	{
		$this->shippingAddress = $shippingAddress;
	}

	/**
	 * @return string
	 */
	public function getDeliveryDate(): string
	{
		return $this->deliveryDate;
	}

	/**
	 * @param string $deliveryDate
	 */
	public function setDeliveryDate(string $deliveryDate): void
	{
		$this->deliveryDate = $deliveryDate;
	}

	/**
	 * @return int
	 */
	public function getHourFrom(): int
	{
		return $this->hourFrom;
	}

	/**
	 * @param int $hourFrom
	 */
	public function setHourFrom(int $hourFrom): void
	{
		$this->hourFrom = $hourFrom;
	}

	/**
	 * @return int
	 */
	public function getHourTo(): int
	{
		return $this->hourTo;
	}

	/**
	 * @param int $hourTo
	 */
	public function setHourTo(int $hourTo): void
	{
		$this->hourTo = $hourTo;
	}

	public function toArray()
	{
		$array = [];

		foreach ($this as $key => $value) {
			$array[$key] = $value;
		}

		return $array;
	}
}