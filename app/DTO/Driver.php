<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 12/18/18
 * Time: 3:33 PM
 */

namespace App\DTO;

/**
 * Class Driver
 *
 * @package App\Domains
 */
class Driver
{
	/** @var string $driverId */
	private $driverId;

	/** @var string $driverName */
	private $driverName;

	/**
	 * @return string
	 */
	public function getDriverId(): string
	{
		return $this->driverId;
	}

	/**
	 * @param string $driverId
	 */
	public function setDriverId(string $driverId): void
	{
		$this->driverId = $driverId;
	}

	/**
	 * @return string
	 */
	public function getDriverName(): string
	{
		return $this->driverName;
	}

	/**
	 * @param string $driverName
	 */
	public function setDriverName($driverName): void
	{
		$this->driverName = $driverName;
	}
}