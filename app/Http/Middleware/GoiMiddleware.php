<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class GoiMiddleware
{
    /**
     * Only want to check that the auth header is set.
     *
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
    	if ($request->hasHeader('Authorization')) {

		    return $next($request);
	    }

	    return response('Unauthorized.', 401);
    }
}
