<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

	/**
	 * @return array
	 */
    protected function contentType(): array
    {
    	return ['Content-Type' => 'application/json', 'UTF-8'];
    }

	/**
	 * @return int
	 */
    protected function successStatusCode(): int
    {
		return 200;
    }
}
