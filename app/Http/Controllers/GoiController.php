<?php

namespace App\Http\Controllers;

use App\Services\GoiService;
use Illuminate\Http\Request;

/**
 * Class GoiController
 *
 * @package App\Http\Controllers
 */
class GoiController extends Controller
{
	/** @var goiService $goiService */
	private $goiService;

	/**
	 * GoiController constructor.
	 *
	 * @param \App\Services\GoiService $goiService
	 */
	public function __construct(GoiService $goiService)
	{
		$this->goiService = $goiService;
	}

	/**
	 * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
	 */
	public function getActiveOrders()
	{
		return response(
			$this->goiService->getActiveOrders(),
			$this->successStatusCode(),
			$this->contentType()
		);
	}

	/**
	 * Active orders related to a driver.
	 *
	 * @param $driverId
	 *
	 * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
	 */
	public function getDriverActiveOrders($driverId)
	{
		return response(
			$this->goiService->getDriverActiveOrders($driverId),
			$this->successStatusCode(),
			$this->contentType()
		);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
	 */
	public function setOrder(Request $request)
	{
		//TODO Take a look on Laravel Request Validator.

		return response(
			$this->goiService->setOrder($request),
			$this->successStatusCode(),
			$this->contentType()
		);
	}

	/**
	 * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
	 */
	public function assignOrders()
	{
		return response(
			$this->goiService->assignOrders(),
			$this->successStatusCode(),
			$this->contentType()
		);
	}
}
