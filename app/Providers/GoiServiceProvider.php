<?php

namespace App\Providers;

use App\Order;
use App\Repositories\DriverRepository;
use App\Repositories\OrderRepository;
use App\Services\GoiService;
use Illuminate\Support\ServiceProvider;

/**
 * Class GoiServiceProvider
 *
 * @package App\Providers
 */
class GoiServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GoiService::class, function ($app) {
        	return new GoiService(
        		new OrderRepository(),
		        new DriverRepository()
	        );
        });
    }
}
