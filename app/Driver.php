<?php

namespace App;

use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Driver extends Model implements AuthorizableContract
{
    use Authorizable;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driverId', 'driverName',
    ];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function orders()
    {
    	return $this->hasMany(Order::class, 'driverId','driverId');
    }
}
