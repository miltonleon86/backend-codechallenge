<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('drivers')->insert(
			[
				'driverId'   => '123',
				'driverName' => 'one',
			]
		);

		DB::table('drivers')->insert(
			[
				'driverId'   => '124f',
				'driverName' => 'two',
			]
		);
		DB::table('drivers')->insert(
			[
				'driverId'   => '125hj',
				'driverName' => 'three',
			]
		);
		DB::table('drivers')->insert(
			[
				'driverId'   => '126gtr',
				'driverName' => 'four',
			]
		);
		DB::table('drivers')->insert(
			[
				'driverId'   => 'sds127',
				'driverName' => 'five',
			]
		);

		DB::table('orders')->insert([
			[
				'orderId'         => 'f1456',
				'driverId'        => '123',
				'status'          => boolval(false),
				'clientName'      => 'Melanie',
				'clientSurname'   => 'Trump',
				'email'           => 'thetrumps@whitehause.com',
				'phone'           => '+492323232',
				'shippingAddress' => '1600 Pennsylvania Ave NW, Washington, DC 20500, EE. UU',
				'deliveryDate'    => \date("Y-m-d"),
				'hourFrom'        => '12',
				'hourTo'          => '20',
			],
		]);

		DB::table('orders')->insert([
			[
				'orderId'         => 'f2457',
				'driverId'        => '123',
				'status'          => boolval(true),
				'clientName'      => 'Donald',
				'clientSurname'   => 'Trump',
				'email'           => 'thetrumps@whitehause.com',
				'phone'           => '+492323232',
				'shippingAddress' => '1600 Pennsylvania Ave NW, Washington, DC 20500, EE. UU',
				'deliveryDate'    => \date("Y-m-d"),
				'hourFrom'        => '12',
				'hourTo'          => '20',
			],
		]);
	}
}
