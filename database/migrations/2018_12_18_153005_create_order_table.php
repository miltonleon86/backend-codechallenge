<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
			$table->string('orderId', 60)->nullable(false)->index();
			$table->string('driverId', 60)->nullable(true)->index();
			$table->boolean('status')->nullable(false)->index();
			$table->string('clientName', 60)->nullable(false);
			$table->string('clientSurname', 60)->nullable(false);
			$table->string('email', 60)->nullable(true);
			$table->string('phone', 60)->nullable(false);
			$table->string('shippingAddress', 100)->nullable(false);
			$table->dateTime('deliveryDate')->nullable(false);
			$table->smallInteger('hourFrom')->nullable(true);
			$table->smallInteger('hourTo')->nullable(true);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
	        $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
