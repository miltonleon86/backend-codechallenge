<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */

/** Own Goi Middleware */
$router->group(['middleware' => 'goiAuth'], function () use ($router) {

	/** Get orders */
	$router->get('/goi/get-orders', [
		'as' => 'goi', 'uses' => 'GoiController@getActiveOrders'
	]);

	/** Get driver orders */
	$router->get('/goi/get-orders/{driverId}', [
		'as' => 'goi', 'uses' => 'GoiController@getDriverActiveOrders'
	]);

	/** Set order */
	$router->post('/goi/set-order', [
		'as' => 'goi', 'uses' => 'GoiController@setOrder'
	]);

	/** Assign Orders */
	$router->put('/goi/assign-orders', [
		'as' => 'goi', 'uses' => 'GoiController@assignOrders'
	]);
});
